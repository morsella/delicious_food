<div class="zigzag container5"></div>
		<footer>
		<div id="copyright">
				<p>Delicious Food &copy; Copyright <?php echo date('Y'); ?>   All Rights Reserved.</p><br />
				<div class="ss-icon">
					<a href="https://www.facebook.com/pages/Delicious-Food/230269663786022" title="Like Delicious Food facebook page" target="_blank">&#xF610;</a>
					<a href="https://twitter.com/DeliciousFoodAm" title="Follow Delicious Food Store on Twitter" target="_blank">&#xF611;</a>
					<a href="https://plus.google.com/+DeliciousFoodAmsterdam/about?gmbpt=true&_ga=1.131540623.1044420185.1448576003" title="Delicious Food on Google&#43;" target="_blank">&#xF613;</a>
					<a href="<?php bloginfo('url'); ?>/contac/" title="Contact Delicious Food  Organic store in Amsterdam" >&#x2709;</a>
				</div>
			</div>
		</footer>
	<?php wp_footer() ;?>
	</body>
</html>

