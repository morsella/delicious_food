jQuery(document).ready(function($){
	 $('.flexslider').flexslider({
    animation: "slide",
    slideshowSpeed: 4000,
    animationSpeed: 5000,
    controlNav: false,

    after: function(slider){
        if(slider.currentSlide >= this.length){
            slider.pause();
            setTimeout(function(){
                slider.play();
            }, 3000);
        }
      }

	});

//____________________MENU_NAVIGATION_SELECTED_LINK__________________________________

	var nav='#menu ul li a';
	var links = '#menu ul li';
	var logo = '.logo_png';
	var pageTitle = document.getElementById('page_title');
	var pageAttr = pageTitle.innerHTML;

	$(nav).each(function() {
		 if($(this).text() == pageAttr) {
			 $(this).parent('li').addClass('selected');
		 }
	})

	$(links).each(function(){
		$(this).on("click", function() {
			    location.href = $(this).children('a').attr("href");
		});
	})

	$(logo).on("click", function(){
		location.href=$(this).children('a').attr("href");

	})

//____________________SLIDE TOGGLE MENU_____________________________________________________

	var theToggle = document.getElementById('toggle');
	$( "#toggle" ).click (function(){
			$(this).toggleClass('on',1000, "easeOut");
			$('#menu').slideToggle('slow');
		return false;
	})

//_____________________NAVIGATION FIXED TOP_____________________________________________________

// var distance = $('h1').offset().top;
var $window = $(window);
var page_url = location.pathname;

$(window).scroll(function() {
                if ($window.scrollTop() > 350) {
                    $('.df_top_navigation').addClass('stuck');
                } else {
                    $('.df_top_navigation').removeClass('stuck');
                }
				if ($(".woocommerce-result-count")[0]){
					$('.woocommerce-breadcrumb').removeClass('bottom_background_creme');
				} else {
				    $('.woocommerce-breadcrumb').addClass('bottom_background_creme');
				}

            });
// console.log(distance);

});



//_____________________COMMENTS FORM_____________________________________________________
jQuery(document).ready(function($){
var edit = '.comment-edit-link';
var says = '.says';
var logged_in = '.logged-in-as';
var comment_notes = '.comment-notes';
// $(edit).before( "<br />" );
$(edit).html( "Edit" );
$(says).html(':');
$( logged_in ).empty();
$(comment_notes).empty();

});

//_____________________CONTACT FORM_____________________________________________________
jQuery(document).ready(function($){
// 	console.log ('Ready');
		$('form.wpcf7-form p textarea').attr("placeholder","Whrite your message");

		$('form.wpcf7-form').submit(function(){
				var hasError = false;

			$('form.wpcf7-form p .error').remove();
			$('form.wpcf7-form p .wpcf7-not-valid-tip').remove();
			$('form.wpcf7-form p input.wpcf7-text, form.wpcf7-form p textarea').each(function(){
			if(jQuery.trim($(this).val()) == '') {
				var labelText = $(this).closest('p').text();
				$(this).parent().append('<span class="error"> Please enter '+labelText+'.</span>');
				hasError = true;
// 				console.log('1');

			} else if($(this).hasClass('df_your_email')) {
				var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
				if(!emailReg.test(jQuery.trim($(this).val()))) {
					var labelText = $(this).closest('p').text();
					$(this).parent().append('<span class="error"> You entered an invalid email address</span>');
					hasError = true;
// 					console.log('2');

				}
			}
		});
	});

});


//_________________SINGLE POST PAGE___________________________________________________
jQuery(document).ready(function($){
	$('.products2 .wp-post-image').removeAttr( "width");
	$('.products2 .wp-post-image').removeAttr("height");
	$('.products2 .product>a').on('click', function( e ){
	   e.preventDefault();
	});


});

//__________________SHOP Page and Categories Pages Title_______________________
jQuery(document).ready(function($){
	var df_page_title  = $('.df_shop h1').text();
	var df_new_title = $('.df_shop h1');
	if(df_page_title == 'Shop'){
		df_new_title.html( 'Delicious Food '+df_page_title );
	}
// 	console.log(df_page_title);
});

//__________________Categories Menu Selected_______________________

jQuery(document).ready(function($){
	var df_product_category = $('.posted_in a').text();
	var df_product_category_menu = $('#categories_product_menu ul li a');
	df_product_category_menu.each(function(){
		if($(this).text() == df_product_category){
			$(this).addClass('df_selected_category');
		}
	});

// 	console.log(df_product_category_menu);
});

//__________________Variations_product_select_____________________
jQuery(document).ready(function($){
	$('.cart .variations .value a').remove();
	$('.cart .variations .value select option:first-child').remove();
});

//_______________Comment_Product_Rating____________________________
jQuery(document).ready(function($){

	var rating = $('.star-rating');
	var ratingList = [];
	rating.each(function(i){
		ratingList[i]= $(this).attr('title');
		if($(this).attr('title') == 'Rated 5 out of 5'){
				$(this).html('<span>&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>');
			}else if($(this).attr('title') == 'Rated 4 out of 5'){
				$(this).html('<span>&#x2605; &#x2605; &#x2605; &#x2605;</span>');
			}else if($(this).attr('title') == 'Rated 3 out of 5'){
				$(this).html('<span>&#x2605; &#x2605; &#x2605;</span>');
			}else if($(this).attr('title') == 'Rated 2 out of 5'){
				$(this).html('<span>&#x2605; &#x2605;</span>');
			}else if($(this).attr('title') == 'Rated 1 out of 5'){
				$(this).html('<span>&#x2605;</span>');
			}else if($(this).attr('title') == 'Rated 1.5 out of 5'){
				$(this).html('<span>&#x2605; &#x2605;</span>');
			}else if($(this).attr('title') == 'Rated 2.5 out of 5'){
				$(this).html('<span>&#x2605; &#x2605; &#x2605;</span>');
			}else if($(this).attr('title') == 'Rated 3.5 out of 5'){
				$(this).html('<span>&#x2605; &#x2605; &#x2605; &#x2605;</span>');
			}else if($(this).attr('title') == 'Rated 4.5 out of 5'){
				$(this).html('<span>&#x2605; &#x2605; &#x2605; &#x2605; &#x2605;</span>');
			}

	});
//			console.log(ratingList);
});



/*
jQuery(document).ready(function($) {
	$('form#contactForm').submit(function() {
		$('form#contactForm .error').remove();
		var hasError = false;
		$('.requiredField').each(function() {
			if(jQuery.trim($(this).val()) == '') {
				var labelText = $(this).prev('label').text();
				$(this).parent().append('<span class="error">You forgot to enter your '+labelText+'.</span>');
				hasError = true;
			} else if($(this).hasClass('email')) {
				var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
				if(!emailReg.test(jQuery.trim($(this).val()))) {
					var labelText = $(this).prev('label').text();
					$(this).parent().append('<span class="error">You entered an invalid email address</span>');
					hasError = true;
				}
			}
		});
		if(!hasError) {
			$('form#contactForm li.buttons button').fadeOut('normal', function() {
				$(this).parent().append('<img src="/wp-content/themes/td-v3/images/loading.gif" alt="Loading&hellip;" height="31" width="31" />');
			});
			var formInput = $(this).serialize();
			$.post($(this).attr('action'),formInput, function(data){
				$('form#contactForm').slideUp("fast", function() {
					$(this).before('<p class="thanks"><strong>Thank you,</strong> Your email was successfully sent. We will get back shortly.</p>');
				});
			});
		}

		return false;

	});

});
*/

