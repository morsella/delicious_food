jQuery(document).ready(function($){
				$("#locations").prepend('<div id="map"></div>');
				$(".static_map").remove();
				var marker;

				function initialize() {
					var mapCanvas = document.getElementById('map');
					var myLatLng = {lat: 52.3789564, lng: 4.88554750000003};
					var image = my_data.template_directory_uri+'/images/map.png';
			        var mapOptions = { center: myLatLng,
			         				   zoom: 18,
			                           mapTypeId: google.maps.MapTypeId.ROADMAP}
			        var map = new google.maps.Map(mapCanvas, mapOptions);
				    var marker = new google.maps.Marker({
				      				map: map,
								    place: { location: myLatLng,
											 query: 'Delicious Food, Amsterdam, Netherlands'},
								    // Attributions help users find your site again.
								    attribution: {
								      source: 'Google Maps JavaScript API',
								      webUrl: 'https://developers.google.com/maps/'},
								      animation: google.maps.Animation.DROP,
									  icon: image
								  });
					    // Construct a new InfoWindow.
					 var infoWindow = new google.maps.InfoWindow({
					    content: 'Delicious Food, <br />'+' Westerstraat 24 1015 MJ '+'Amsterdam  Netherlands'
					  });

					  // Opens the InfoWindow when marker is clicked.
					  marker.addListener('click', function() {
					    infoWindow.open(map, marker);
					    marker.setAnimation(google.maps.Animation.BOUNCE);

						});
					}



      google.maps.event.addDomListener(window, 'load', initialize);

});
