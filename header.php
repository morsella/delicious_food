<!DOCTYPE html>
<html>
	<head>

		<title>
			<?php

				wp_title( '-', true, 'right' );

				bloginfo( 'name' );

			?>
		</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">		<?php wp_head(); ?>
	</head>
	<body>
	<header>
			<!-- FLEX SLIDER -->
	            <div id="slider">
		            <div class="flexslider">

		                <ul class="slides pointer_events">
		                    <li class="slide slide1"><img src="<?php echo get_bloginfo('template_directory');?>/images/slider05.jpg" alt="working_table_01"  /></li>
		                    <li class="slide slide2"><img src="<?php echo get_bloginfo('template_directory');?>/images/slider06.jpg" alt="working_table_02"  /></li>
		                    <li class="slide slide3"><img src="<?php echo get_bloginfo('template_directory');?>/images/slider07.jpg" alt="working_table_03"  /></li>
		                    <li class="slide slide4"><img src="<?php echo get_bloginfo('template_directory');?>/images/slider08.jpg" alt="working_table_04"  /></li>
		                </ul>
		            </div>
	            </div>
	        <!-- End Slider -->

	        <!-- NAVIGATION -->
	            <nav class="df_top_navigation">

	            <a href="#menu" id="toggle"><span></span></a>
					<div id="menu">
						<?php
								if( is_home() && get_option('page_for_posts') ) {
									$blog_page_id = get_option('page_for_posts');
									echo '<span id="page_title">'.get_page($blog_page_id)->post_title.'</span>';
								}else if(is_shop()){
									$pageTitle = "Shop";
									echo '<span id="page_title">'.$pageTitle.'</span>';
								}else{
									$pageTitle = get_the_title();
									echo '<span id="page_title">'.$pageTitle.'</span>';
								}
?>
						<ul>
								<?php

								$args = array(
									'menu' => 'main-menu',
									'echo' => false
								);
								$navMenu =wp_nav_menu( $args );
								$stripTags=strip_tags($navMenu, '<li><a>');
								echo $stripTags;


							?>
							</ul>
						</div>

	            </nav>
	        <!-- End Navigation -->


				<div class="logo_png" title="Delicious Food">
				<a class="logo_img_link" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="delicious food home page"></a>
					<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/logo.png" alt="Delicious Food" /></div>

	</header>
	<br>
