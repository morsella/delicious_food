	<div class="products">
			<?php
        $args = array(
            'post_type'      => 'product',
            'posts_per_page' => 2,
            'meta_query'     => array(
                    'relation' => 'OR',
                    array( // Simple products type
                        'key'           => '_sale_price',
                        'value'         => 0,
                        'compare'       => '>',
                        'type'          => 'numeric'
                    ),
                    array( // Variable products type
                        'key'           => '_min_variation_sale_price',
                        'value'         => 0,
                        'compare'       => '>',
                        'type'          => 'numeric'
                    )
                )
        );
        $loop = new WP_Query( $args );
        if ( $loop->have_posts() ) {
            while ( $loop->have_posts() ) : $loop->the_post();
?>
				<div class="product">
						 <h3><?php the_title(); ?></h3>

                         <a id="id-<?php the_id(); ?>"
                         	href="<?php the_permalink(); ?>"
                         	title="<?php the_title(); ?>">

                            <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'"  />'; ?>
                            <div class="view">
                            	<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/eye.png" alt="view more about the product at Delicious Food" />
                            </div>

                        </a>
                        <p><?php echo the_field('short_description'); ?></p>
						<span class="price"><?php echo $product->get_price_html(); ?></span>
						<a class="btn" href="<?php the_permalink(); ?>"
							title="<?php echo the_title(); ?>">View Product</a>
					 <?php woocommerce_template_loop_add_to_cart( $loop->post, $product ); ?>

				</div>
        <?php endwhile;
                } else {
            echo __( 'No products found' );
        }
		?>
        <?php wp_reset_query(); ?>
	</div>
