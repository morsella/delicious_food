<?php
	//set variables
	$nameError = '';
	$emailError = '';
	$commentError = '';

	//if the form was submited validate it?
if(isset($_POST['submitted'])) {


	//Check to see if the honeypot captcha field was filled in
	if(trim($_POST['checking']) !== '') {
		$captchaError = true;
	} else {

		//Check to make sure that the name field is not empty
		if(trim($_POST['contactName']) === '') {
			$nameError = 'You forgot to enter your name.';
			$hasError = true;
		} else {
			$name = trim($_POST['contactName']);
		}

		//Check to make sure sure that a valid email address is submitted
		if(trim($_POST['email']) === '')  {
			$emailError = 'You forgot to enter your email address';
			$hasError = true;
		} else if (!filter_var(trim($_POST['email']), FILTER_VALIDATE_EMAIL)) {
			$emailError = 'You entered an invalid email address';
			$hasError = true;
		} else {
			$email = trim($_POST['email']);
		}

		//Check to make sure comments were entered
		if(trim($_POST['comments']) === '') {
			$commentError = 'You forgot to write your message';
			$hasError = true;
		} else {
			if(function_exists('stripslashes')) {
				$comments = stripslashes(trim($_POST['comments']));
			} else {
				$comments = trim($_POST['comments']);
			}
		}

		//If there is no error, send the email
		if(!isset($hasError)) {
			$adminEmail = get_option( 'admin_email' );
			$emailTo = $adminEmail.', contact.deliciousfood@gmail.com,  	contact@deliciousfood.nl';
			$subject = 'Contact Form Submission from '.$name;
			$sendCopy = trim($_POST['sendCopy']);
			$body = "Name: $name \n\nEmail: $email \n\nComments: $comments";
			$message = '<html><body>';
			$message .= '<h1>Hello, World!</h1>';
			$message .= '</body></html>';
			$headers = 'From: Delicious Food Site <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;
			$headers .= "CC: susan@example.com\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

			mail($emailTo, $subject, $body, $message, $headers);

			if($sendCopy == true) {
				$subject = 'You emailed Delicious Food';
				$body .= nl2br('\r\n Please do not reply with this email \r\n To contact us please follow the link to our website \r\n http://deliciousfood.nl/contact/ ');
				$message = '<html><body>';
				$message .= '<h1>Hello, World!</h1>';
				$message .= '</body></html>';
				$headers = 'From:  <contact@deliciousfood.nl>';
				$headers .= "CC: susan@example.com\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

				mail($email, $subject, $body, $message , $headers);

			}

			$emailSent = true;

		}
	}
}

//if the email was sent, show a thank you message or show the form
if(isset($emailSent) && $emailSent == true){
	echo '<div class="thanks"><h1> Thank you, '.$name.' </h1>';
	echo '<p>Your email was sent successfully. We will be in touch shortly </p>';
	echo '</div>';

}else{
	if(have_posts()): while(have_posts()): the_post();
	echo '<h1>'.the_title().'</h1>';
	echo '<div class="zigzag container"></div>';
	echo '<section id="selection2">'.the_content();

	if(isset($hasError) || isset($captchaError)) {
		echo '<p class="error">There was an error submitting the form</p>';

	}
	echo '<form action="'.$_SERVER['REQUEST_URI'].'" id="contactForm" method="post">';
	echo '<ol class="forms">';
	echo '<li><label for="contactName">Your Name </label>';
	echo '<input type="text" name="contactName" id="contactName" value="';
	if(isset($_POST['contactName'])){
			echo $_POST['contactName'];
		}
	echo '" class="requiredField" />';
			if($nameError != ''){
				echo '<span class="error">'.$nameError.'</span>';
			}
	echo '</li>';
	echo '<li><label for="email">Your Email </label>';
	echo '<input type="text" name="email" id="email" value="';
	if(isset($_POST['email'])){
			echo $_POST['email'];
		}
	echo '"  class="requiredField" />';
			 if($emailError != '') {
					echo '<span class="error">'.$emailError.'</span>';
			}
	echo '</li>';
	echo '<li class="textarea"><label for="commentsText">Your Message </label>';
	echo '<textarea name="comments" id="commentsText" row="40" cols="40" class="requiredField">';
	if(isset($_POST['comments'])){
			echo $_POST['comments'];
		}
	echo '</textarea>';
			if($commentError != ''){
				echo '<span class="error">'.$commentError.'</span>';
			}

	echo '</li>';
	echo '<li class="inline"><input type="checkbox" name="sendCopy" id="sendCopy" value="true"';
			if(isset($_POST['sendCopy']) && $_POST['sendCopy'] == true){
				echo 'checked="checked"';
			}
	echo '/><label for="sendCopy">Send a copy of this email to yourself</label></li>';
	echo '<li class="screenReader"><label for="checking" class="screenReader">If your want to submit this form, do not enter anything in this field</label>';
	echo '<input type="text" name="checking" id="checking" class="screenReader" value="';
	if(isset($_POST['checking'])){
			echo $_POST['checking'];
		}
	echo '" /></li>';
	echo '<li class="buttons"><input type="hidden" name="submitted" id="submitted" value="true" />';
	echo '<button type="submit">Send your message &raquo;</button></li>';
	echo '</ol></form>';
	echo '</section>';
	endwhile; endif;
}















