<?php
require get_template_directory() . '/inc/register_activation.php';

// Enable custom menus
add_theme_support( 'menus' );
add_theme_support('post-thumbnails');


// Load the Theme CSS
function theme_styles() {

	wp_enqueue_style( 'social', get_template_directory_uri() . '/css/social/ss-social.css' );
	wp_enqueue_style( 'flexslider', get_template_directory_uri() . '/css/flexslider.css' );
	wp_enqueue_style( 'main', get_template_directory_uri() . '/style.css' );

}

// Load the Theme JS
function theme_js() {
	wp_register_script('new_service', get_template_directory_uri() . '/js/modernizr.js', array('jquery'), true);
    wp_enqueue_script('new_service');

	wp_register_script( 'flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js', array('jquery'), '', true );
	wp_enqueue_script( 'flexslider' );
	wp_register_script('theme_js', get_template_directory_uri() . '/js/theme.js', array('jquery'), true);
	wp_enqueue_script( 'theme_js');
}



// Load google maps
function google_maps_api() {
		wp_register_script( 'googlemap', 'https://maps.googleapis.com/maps/api/js?',array('jquery'),'',true);
		wp_register_script( 'elevation_service', 'https://www.google.com/jsapi',true);
		wp_register_script( 'maps', get_template_directory_uri() .'/js/maps.js', array('jquery'),'',true);
		if( is_page( 'home' ) || is_page( 'about' ) || is_page( 'contact' ) ) {
		wp_enqueue_script('googlemap');
		wp_enqueue_script('elevation_service');
		wp_enqueue_script('maps');
	}
}

//Set directory in JS files
function set_js_var() {
  $translation_array = array( 'template_directory_uri' => get_template_directory_uri());
  wp_localize_script( 'jquery', 'my_data', $translation_array );
}


add_action( 'wp_enqueue_scripts', 'theme_js' );
add_action( 'wp_enqueue_scripts', 'google_maps_api' );
add_action('wp_enqueue_scripts','set_js_var');
add_action( 'wp_enqueue_scripts', 'theme_styles' );

// Add widgets
function create_widget($name, $id, $description){
	$args = array(
		'name'          => __( $name),
		'id'            => $id,
		'description'   => $description,
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>'
	);

	register_sidebar($args);

}
create_widget('Left Footer', 'footer_left', 'Displays in the bottom left of the footer');
create_widget('Middle Footer', 'footer_middle', 'Display in the bottom middle of the footer');
create_widget('Right Footer', 'footer_right', 'Display in the bottom right of the footer');


add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}


//Post Views

function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

// Remove issues with prefetching adding extra views
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


//Remove URl from comments
function remove_comment_fields($fields) {
    unset($fields['url']);
    return $fields;
}
add_filter('comment_form_default_fields','remove_comment_fields');

//___VALIDATION for the Comments form
// don't call the file directly // Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

function df_comment_validation_init() {

	//load files only in single posts & if comments are open
	if(is_singular() && comments_open() ) {

	//loading plugin assest (CSS & JS files)

// 	wp_enqueue_style( 'df_validation_style', get_template_directory_uri() . '/css/validation.css' );
	wp_enqueue_script('validate_min', get_template_directory_uri() . '/js/jquery.validate.min.js', array('jquery'));
	wp_enqueue_script('df_validation', get_template_directory_uri() . '/js/validate.js', array('jquery','validate_min'));
	}
}
add_action('wp_footer', 'df_comment_validation_init');
//____END VALIDATION_____




/**
 * Chanege wp_die
 */
function my_custom_die_handler($message, $title='', $args=array())
{
  // Get the path to the custom die template
  $template = get_theme_root().'/'.get_template().'/custom_die.php';

  // If not an admin page, and if the custom die template exists
  if( ! is_admin() && file_exists( $template ) )
  {
    // The default response code is 500
    $defaults = array( 'response' => 500 );

    // Combine the args and defaults
    $r = wp_parse_args( $args, $defaults );

    // Determine what type of error we are dealing with
    if( function_exists( 'is_wp_error' ) && is_wp_error( $message ) )
    {
      // If no title, try to make one
      if ( empty( $title ) )
      {
        $error_data = $message->get_error_data();

        if ( is_array( $error_data ) && isset( $error_data['title'] ) )
          $title = $error_data['title'];
      }

      // Get the actual errors
      $errors = $message->get_error_messages();

      // Depending on how many errors there are, do something
      switch ( count( $errors ) )
      {
        case 0 :
          $message = '';
          break;
        case 1 :
          $message = '<li>' . $errors[0] . '</li>';
          break;
        default :
          $message = '
            <li>' . join( "</li><li>", $errors ) . '</li>
          ';
          break;
      }

    }
    else if( is_string( $message ) )
    {
      $message = '<li>' . $message . '</li>';
    }



    /**
     * Since wp-comments-post.php does not allow error
     * messages to be changed, the error message string
     * is run through str_replace to customize.
     */
    $lame_messages = array(
      'Sorry, comments are closed for this item.',
      'please fill the required fields (name, email).',
      'please enter a valid email address.',
      'please type a comment.',
    );

    $replacements = array(
      'Sorry, comments are closed.',
      'All comment form fields are required. Please try again.',
      'Please enter a valid email address.',
      'No comment in your submission. Please try again.',
    );

    // Save the original error message for later comparison
    $pre_replacements = $message;

    // Do the comment form error message replacements
    $message = str_replace( $lame_messages, $replacements, $message );

    if(
      // If the back link was set in the args
      ( isset( $r['back_link'] ) && $r['back_link'] ) OR

      // Or if message identified as comment error
      $message !== $pre_replacements
    )
    {
      // Show the back button
      $back_link = '
        <p>
          <a href="javascript:history.back()" class="back">Back</a>
        </p>
      ';
    }

    // If the title is empty, set it as "Error"
    if( empty( $title ) )
      $title = 'Error';

    require_once( $template );

    die();

  }
  else
  {
    _default_wp_die_handler( $message, $title, $args );
  }
}

// Intermediate function is necessary to customize wp_die
function swap_die_handlers()
{
  return 'my_custom_die_handler';
}

add_filter('wp_die_handler', 'swap_die_handlers');


