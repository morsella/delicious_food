<?php register_activation_hook( __FILE__, 'df_new_table' );

	function df_new_table()
{
    global $wpdb;

    $table_name = $wpdb->prefix . 'df_new_table';

    $sql = "CREATE TABLE $table_name (
      id int(11) NOT NULL AUTO_INCREMENT,
      name varchar(255) DEFAULT NULL,
      UNIQUE KEY id (id)
    );";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}
