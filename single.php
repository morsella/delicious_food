<?php get_header(); ?>

<section>
<div class="title">
						<div>
							<h1>Current Post: <?php the_title(); ?></h1>
							<h2>Delicious Food Posting</h2>
						</div>
						<div class="border_png">
								<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/border.png" alt="Delicious Food Store content devider" />
						</div>
</div>
</section>
		<div class="products2">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'post' ); ?>
		<?php
          setPostViews(get_the_ID());
		?>

	<?php endwhile; else: ?>

		<p>There are no posts or pages here</p>

	<?php endif; ?>
		</div>
		<?php comments_template(); ?>

<?php get_footer(); ?>