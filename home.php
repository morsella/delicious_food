<?php get_header(); ?>
<section>
<div class="title">
						<div>
							<h1>Our Blog Posts</h1>
							<h2>Delicious Food Posting</h2>
						</div>
						<div class="border_png">
								<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/border.png" alt="Delicious Food Store content devider" />
						</div>
						<div class="short_description">
								<p>Take your time and enjoy our recent posts. We publish New Arrivals and Delicious Recipes, Great gift ideas and much more... </p>
						</div>
</div>
</section>
<div class="zigzag container"></div>
	<section id="selection2">
		<h5>Pick a Category</h5>
			<div id="categories_blog_menu">
				<ul>
				<?php
				$args = array(
				  'orderby' => 'name',
				  'order' => 'ASC'
				  );
				$categories = get_categories($args);
				  foreach($categories as $category) {
				    echo '<li> <a href="' . get_category_link( $category->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a> </li> '; }
				?>
				</ul>
			</div>
		<div class="products">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'post' ); ?>

				<!-- <hr> -->

			<?php endwhile; else: ?>

				<p>There are no posts to display</p>

			<?php endif;
				the_posts_pagination( array(
					'mid_size'  => 2,
					'prev_text' => __( 'Prev', 'textdomain' ),
					'next_text' => __( 'Next', 'textdomain' ),
				) );



			?>

		</div>
	</section>
<?php get_footer(); ?>