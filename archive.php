<?php get_header(); ?>
<section>
<div class="title">
						<div>
							<h1> <?php single_month_title(' '); ?></h1>
							<h2>Archive Page to help you view the Recent Posts</h2>
						</div>
						<div class="border_png">
								<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/border.png" alt="Delicious Food Store content devider" />
						</div>
						<div class="short_description">
								<p>Take your time and enjoy our recent posts. We publish New Arrivals and Delicious Recipes, Great gift ideas and much more... </p>
						</div>
</div>
</section>
<div class="zigzag container"></div>

	<section id="selection2">
		<h5>Pick a Month</h5>
			<div id="categories_blog_menu">
				<ul>

				<?php
				$args = array(
				  	'type'            => 'monthly',
				  	'limit'           => '',
				  	'format'          => 'html',
				  	'before'          => '',
				  	'after'           => '',
				  	'show_post_count' => false,
				  	'echo'            => 1,
				  	'order'           => 'DESC'

				  );
				$archive = wp_get_archives($args);
				?>
				</ul>
			</div>


		<div class="products">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php include('content-post.php'); ?>
	       <?php endwhile; else: ?>
		        <p>There are no posts or pages here</p>
		        <?php endif; ?>
        <?php wp_reset_query(); ?>

</div>
	</section>
<?php get_footer(); ?>