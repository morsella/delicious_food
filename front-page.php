<?php get_header(); ?>
<section id="selection1">
			<div class="title">
						<div>
							<h1>Delicious Food</h1>
							<h2>Biologische Delicatesse Winkel</h2>
						</div>
						<div class="border_png">
								<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/border.png" alt="Delicious Food Store content devider" />
						</div>
						<div class="short_description">
								<p>Welkom op de website van Delicious Food. De biologische winkel in het hart van de Jordaan. Waar we kwaliteitsproducten bieden die het lichaam en de geest een extra boost geven. Kom gerust eens langs voor de geur en smaak van echt eten.</p>
						</div>
			</div>
			<div class="circles ">
							<div class="circle">
								<h3>Kwaliteit</h3>
								 <img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/apple.jpg" alt="Producten van de hoogste kwaliteit" />
								 <p>Onze producten worden met zorg ingekocht. Altijd vers en van de hoogste kwaliteit om optimaal te genieten</p>

							</div>
							<div class="circle">
								<h3>Duurzaam</h3>
								<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/hands.jpg" alt="Duurzaam Producten bij Delicious Food" />
								<p>Duurzaam geproduceerd voedsel dat lekker en goed smaakt, boordevol gezonde vitamines en mineralen</p>

							</div>
							<div class="circle">
								<h3>Biologisch</h3>
								<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/vegies.jpg" alt="100% biologische producten" />
								<p>Een compleet assortiment met de zekerheid dat alle producten op een biologische wijze tot stand komen</p>

							</div>
			</div>
</section>
<div class="zigzag container"></div>
	<section id="selection2">
		<h4>New Posts</h4>
		<h5>We Provide Amazing Products</h5>
		<div class="border_png">
			<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/border.png" alt="Delicious Food Store content devider" /></div>
			<div class="products">
							<?php
								$args = array(
									'post_type' => 'post',
									'cat' => -1,
									'posts_per_page' => 2
								);

								$the_query = new WP_Query( $args );
								if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();

								 get_template_part( 'content', 'post' );
							 ?>
						<?php
							endwhile; endif;
						?>
			</div>

		<!--
		//For new products
		<?php include_once get_template_directory()."/inc/views/recent_products.php" ?>

		 -->

	</section>
	<div class="zigzag container3"></div>
	<section id="selection3">
				<h4>Our Service</h4>
				<h5>Why You Choose Delicious Food</h5>
				<div class="border_png">
					<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/border.png" alt="Delicious Food Store content devider" /></div>

			<div class="circles2 ">
							<div class="circle2">
								<h6>Best Recipes</h6>
								 <img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/recipe.jpg" alt="Producten van de hoogste kwaliteit" />
								 <p>Onze producten worden met zorg ingekocht. Altijd vers en van de hoogste kwaliteit om optimaal te genieten</p>

							</div>
							<div class="circle2">
								<h6>Friendly Staff</h6>
								<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/stuff.jpg" alt="Duurzaam Producten bij Delicious Food" />
								<p>Duurzaam geproduceerd voedsel dat lekker en goed smaakt, boordevol gezonde vitamines en mineralen check for more info</p>

							</div>
							<div class="circle2">
								<h6>100% Organic</h6>
								<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/lief.jpg" alt="100% biologische producten" />
								<p>Een compleet assortiment met de zekerheid dat alle producten op een biologische wijze tot stand komen</p>

							</div>
							<div class="circle2">
								<h6>Fresh and Delicious</h6>
								<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/cutlery.jpg" alt="100% biologische producten" />
								<p>Een compleet assortiment met de zekerheid dat alle producten op een biologische wijze tot stand komen</p>

							</div>
			</div>



	</section>
<!--
	<section id="sale">
	<?php include_once get_template_directory()."/inc/views/products_on_sale.php" ?>
	</section>
-->
		<div class="zigzag container4"></div>
	<section id="selection4">
		<h5>Our Location</h5>
		<div id="locations"></div>
		<div id="map_details">
							<address class="bussiness_map_details">
							<p class="df_address">Address</p>
								Delicious Food <br />
								Westerstraat 24, 1015 MJ  <br /> Amsterdam, Netherlands

							</address>

							<div class="bussiness_map_details">
							<h6>Opening Hours</h6>
								<p>     MA tot/met VRI 09:30 - 18:30 <br/>
										ZA 09:30 - 18:00<br/>
										ZO Gesloten</p>
							</div>
	</div>
							<div class="phone">
							<img id="phone" src="<?php echo get_bloginfo('template_directory');?>/images/phone.svg" onerror="this.onerror=null; this.src='<?php echo get_bloginfo('template_directory');?>/images/phone.png'" alt="the phone icone" /> 020 320 3070

	</div>

	</section>

<?php get_footer(); ?>
