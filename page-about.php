<?php get_header(); ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<h1><?php the_title() ;?></h1>
				<h2>Bringing the Best since 1996</h2>

				<div class="border_png">
								<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/border.png" alt="Delicious Food Store content devider" /></div>
				<div class="short_description">
					<p>Our store is located at the heart of Amsterdam. Easy access by bicycle, car, public transport or even walking distance.
						Long and flexible working hours help you getting your favourite fresh organic goodies at any time during a day.</p>
				</div>
<!-- <div class="zigzag container"></div> -->
				<div class="page_content">
					<div class="wp_description_bg">
						<div class='wp_about_description'><!-- MAX 100 WORDS  -->
				<?php the_content(); ?>
						</div>
					</div>
				</div>


			<?php endwhile; else: ?>

				<p>There are no posts or pages here</p>

			<?php endif; ?>
		<div class="zigzag container"></div>
			<section id="selection2">
						<h4>Meet Our Staff</h4>
							<h5>We are here to help you find what you need</h5>
						<div class="border_png">
									<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/border.png" alt="Delicious Food Store content devider" />
						</div>
						<div class="short_description">
							<p>From generation to generation, we check the quality and bring the best affordable organic products. Our professional team can always spoil you with new arrivals and delicious snacks.</p>
						</div>
									<div class="circles_staff">
							<div class="circle_staff">
								<h3>Jenna Doe</h3>
								 <img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/staff.jpg" alt="Producten van de hoogste kwaliteit" />
								 <p>Onze producten worden met zorg ingekocht. Altijd vers en van de hoogste kwaliteit om optimaal te genieten</p>

							</div>
							<div class="circle_staff">
								<h3>John Doe</h3>
								<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/staff.jpg" alt="Duurzaam Producten bij Delicious Food" />
								<p>Duurzaam geproduceerd voedsel dat lekker en goed smaakt, boordevol gezonde vitamines en mineralen</p>

							</div>
							<div class="circle_staff">
								<h3>Maria Best</h3>
								<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/staff.jpg" alt="100% biologische producten" />
								<p>Een compleet assortiment met de zekerheid dat alle producten op een biologische wijze tot stand komen</p>

							</div>
			</div>
			</section>
		<div class="zigzag container4"></div>

<!-- <?php get_template_part( 'content', 'testimonials' ); ?> -->
	<section id="selection4">
		<h5>Our Location</h5>
		<div id="locations"></div>
		<div id="map_details">
							<address class="bussiness_map_details">
							<p class="df_address">Address</p>
								Delicious Food <br />
								Westerstraat 24, 1015 MJ  <br /> Amsterdam, Netherlands

							</address>

							<div class="bussiness_map_details">
							<h6>Opening Hours</h6>
								<p>     MA tot/met VRI 09:30 - 18:30 <br/>
										ZA 09:30 - 18:00<br/>
										ZO Gesloten</p>
							</div>
	</div>
									<div class="phone">
							<img id="phone" src="<?php echo get_bloginfo('template_directory');?>/images/phone.svg" onerror="this.onerror=null; this.src='<?php echo get_bloginfo('template_directory');?>/images/phone.png'" alt="the phone icone"/> 020 320 3070

	</div>

	</section>


<?php get_footer(); ?>

