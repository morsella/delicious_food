<?php

/**
 * The template for displaying Comments
 *
 * The area of the page that contains comments and the comment form.
 *
 */

/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() )
	return;
?>
<div class="zigzag container"></div>
<section id="selection2">

		<div id="comments" class="comments-area">
	<?php if ( have_comments() ) : ?>
		<h2 class="comments-title">
			<?php
				printf( _nx( 'One thought on &ldquo;%2$s&rdquo;', '%1$s thoughts on &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'delicious_food' ),
					number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
			?>
		</h2>
		<div class="border_png">
								<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/border.png" alt="Delicious Food Store content devider" />
						</div>

		<div class="comment-list">
			<?php
				wp_list_comments( array(
					'style'       => 'div',
					'short_ping'  => true,
					'avatar_size' => 30,

				) );
			?>
		</div></div><!-- .comment-list -->

		<?php
			// Are there comments to navigate through?
			if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
		?>
		<div id="comment_pagination" class="navigation comment-navigation" role="navigation">
			<h6 class="screen-reader-text section-heading"><?php _e( 'Comment navigation', 'delicious_food' ); ?></h6>
			<div class="nav-previous"><?php previous_comments_link( __( '&larr; Follow the link for Older Comments', '0' ) ); ?></div>
			<div class="nav-next"><?php next_comments_link( __( ' Follow the link for Newer Comments &rarr;', 'delicious_food' ) ); ?></div>
		</div><!-- .comment-navigation -->
		<?php endif; // Check for comment navigation ?>

		<?php if ( ! comments_open() && get_comments_number() ) : ?>
		<p class="no-comments"><?php _e( 'Comments are closed.' , 'delicious_food' ); ?></p>
		<?php endif; ?>

	<?php endif; // have_comments() ?>


	<?php
		//	Check if user logged in if not link to log in and redirect to curent link

		if ( is_user_logged_in() ) {
	$current_user = wp_get_current_user();
	echo '<p id="welcome_user">Welcome, '. $current_user->user_login .'!</p>';
// 	echo '<a href="'.wp_logout_url( get_permalink() ) .'">Logout</a>';
} else {
	echo '<p id="welcome_user">Welcome, Visitor!</p>';
// 	echo '<a href="'.wp_login_url( get_permalink() ). '" title="Login">Login</a>';
}
	?>


<?php
$commenter = wp_get_current_commenter();
$req = get_option( 'require_name_email' );
$aria_req = ( $req ? " aria-required='true'" : '' );
$fields =  array(
    'author' => '<p class="comment-form-author">' . ( $req ? '<span class="required">Name * </span>' : '' ) .
        '<input id="author" name="author" type="text" placeholder="Your Name" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',
    'email'  => '<p class="comment-form-email">' . ( $req ? '<span class="required">Email * </span>' : '' ) .
        '<input id="email" name="email" type="text" placeholder="Your Email" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p>',

);


$comments_args = array(
    'fields' =>  $fields,
    'title_reply'=>'Please give us your valuable comment',
    'label_submit' => 'Send My Comment',
    'comment_field' =>  '<p class="comment-form-comment"><label for="comment">We respect your privacy. &nbsp; Please write your ' . _x( 'comment', 'noun' ) .
    ' below * </label><br /><textarea id="comment" name="comment" cols="40" rows="10" aria-required="true" placeholder="Write your comment here">' .
    '</textarea></p>',
);


comment_form($comments_args);
?>


</div>
</section>




