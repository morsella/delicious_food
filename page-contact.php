<?php
/*
	Template Name: Contact Delicious Food
*/

// short code for Fast Secure Contact form [si-contact-form form='1']
//import contact_form.php
get_header();
?>

<h1>Contact Us</h1>

<h2>Biologische Delicatesse Winkel</h2>
						<div class="border_png">
								<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/border.png" alt="Delicious Food Store content devider" />
						</div>
						<div class="short_description">
								<p>Welkom op de website van Delicious Food. De biologische winkel in het hart van de Jordaan. Waar we kwaliteitsproducten bieden die het lichaam en de geest een extra boost geven. Kom gerust eens langs voor de geur en smaak van echt eten.</p>
						</div>

<div class="display_box_height">
</div>
<div class="zigzag container3"></div>
<section id="selection3">
<h6>All fields are required</h6>

<?php echo do_shortcode( '[contact-form-7 id="100" title="Contact form 1"]' ); ?><br />

<div class="clear_all_floats"></div>
</section>
		<div class="zigzag container4"></div>
	<section id="selection4">
		<h5>Our Location</h5>
		<div id="locations"></div>
		<div id="map_details">
							<address class="bussiness_map_details">
							<p class="df_address">Address</p>
								Delicious Food <br />
								Westerstraat 24, 1015 MJ  <br /> Amsterdam, Netherlands

							</address>

							<div class="bussiness_map_details">
							<h6>Opening Hours</h6>
								<p>     MA tot/met VRI 09:30 - 18:30 <br/>
										ZA 09:30 - 18:00<br/>
										ZO Gesloten</p>
							</div>
	</div>
									<div class="phone">
							<img id="phone" src="<?php echo get_bloginfo('template_directory');?>/images/phone.svg" onerror="this.onerror=null; this.src='<?php echo get_bloginfo('template_directory');?>/images/phone.png'" alt="phone icone" /> 020 320 3070

	</div>

	</section>


<?php get_footer(); ?>

