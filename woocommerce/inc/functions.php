<?php /**
 *
 *
 * @package DeliciousFood Woocommerce
 */

/**
 * Initialize all the things.
 */
	remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);
// 	remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
	remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
	remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);
	remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
	remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
// 	remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10);



	add_action('df_categories_products', 'df_categories_product_list');
	add_action('woocommerce_after_single_product_summary','comments_template', 30);
	add_filter( 'woocommerce_sale_flash', 'wc_custom_replace_sale_text' );


function wc_custom_replace_sale_text( $html ) {
    return str_replace( __( 'Sale!', 'woocommerce' ), __( 'Sale', 'woocommerce' ), $html );
}

function df_categories_product_list(){

		  $taxonomy     = 'product_cat';
		  $orderby      = 'name';
		  $show_count   = 0;      // 1 for yes, 0 for no
		  $pad_counts   = 0;      // 1 for yes, 0 for no
		  $hierarchical = 1;      // 1 for yes, 0 for no
		  $title        = '';
		  $empty        = 0;

		  $args = array(
		         'taxonomy'     => $taxonomy,
		         'orderby'      => $orderby,
		         'show_count'   => $show_count,
		         'pad_counts'   => $pad_counts,
		         'hierarchical' => $hierarchical,
		         'title_li'     => $title,
		         'hide_empty'   => $empty
		  );
		 $all_categories = get_categories( $args );
				 foreach ($all_categories as $cat) {
						    if($cat->category_parent == 0) {
						        $category_id = $cat->term_id;
						        echo '<li><a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a></li>';

						/*
						        $args2 = array(
						                'taxonomy'     => $taxonomy,
						                'child_of'     => 0,
						                'parent'       => $category_id,
						                'orderby'      => $orderby,
						                'show_count'   => $show_count,
						                'pad_counts'   => $pad_counts,
						                'hierarchical' => $hierarchical,
						                'title_li'     => $title,
						                'hide_empty'   => $empty
						        );
						        $sub_cats = get_categories( $args2 );
						        if($sub_cats) {
						            foreach($sub_cats as $sub_category) {
						                echo  $sub_category->name ;
						            }
						        }
						*/
						  	  }
					}
}
//_____ do_action('your_action_unique_name');________
//Add the action to the page where you want it to display



