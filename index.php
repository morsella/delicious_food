<?php get_header(); ?>



	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<h1><?php the_title() ;?></h1>

		<?php get_template_part( 'content', 'post' ); ?>

		<!-- <hr> -->

	<?php endwhile; else: ?>

		<p>There are no posts to display</p>

	<?php endif; ?>
<?php get_footer(); ?>