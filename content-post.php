<div class="product">
						 <h3><?php the_title(); ?></h3>
						<?php if ( has_post_thumbnail() ) : ?>
						<a id="id-<?php the_id();?>" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
						<?php the_post_thumbnail(); ?>
						<?php endif; ?>
                            <div class="view">
                            	<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/eye.png" alt="view more about the product at Delicious Food" />
                            </div>
						</a>
			<ul>
				<li>Posted in:  <?php the_category(', '); ?></li>
<!-- 				<li><lable>Written by: </lable><a href="/about/"><?php the_author(); ?></a></li> -->

          		<li>Post Views: <a href="<?php the_permalink(); ?>"><?php echo getPostViews(get_the_ID()) ?></a></li>


				<?php
					$archive_year  = get_the_time('Y');
					$archive_month = get_the_time('m');
					$archive_day   = get_the_time('d');
?>
				<li>On: <a href="<?php echo get_month_link( $archive_year, $archive_month ); ?>">
 <?php the_time('F Y'); ?>
</a></li>
			</ul>
			<div class="excerpt">
				<?php if(is_single()): ?>
					<?php the_content(); ?><br />
				<?php else: ?>
					<?php the_excerpt(); ?>
					<a class="btn" href="<?php the_permalink(); ?>"
							title="<?php echo the_title(); ?>">View Post</a>
				<?php endif; ?>
			</div>
</div>

