<?php get_header(); ?>

<section>
<div class="title">
						<div>
							<h1>Something went wrong <?php the_title(); ?></h1>
							<h2>Delicious Food</h2>
						</div>
						<div class="border_png">
								<img class="pointer_events" src="<?php echo get_bloginfo('template_directory');?>/images/border.png" alt="Delicious Food Store content devider" />
						</div>
</div>
</section>

		  <!-- The message and back link go somewhere in the body -->
  <ul class="df_messege">
    <?php echo $message; ?>
  </ul>
  <?php
    // Do we need a back button?
    if( isset( $back_link ) )
    {
      echo '<p>' . $back_link . '</p>';
    }
  ?>


<?php get_footer(); ?>
